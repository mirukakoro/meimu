from panda3d.bullet import BulletTriangleMesh, BulletTriangleMeshShape, BulletRigidBodyNode
from direct.task import TaskManagerGlobal
import ou


import random

config = ou.config_.load()


ou.prc.load()
app = ou.base.app()
# app.disable_mouse()
bullet_world = ou.bullet.world(app)
etc = ou.etc.etc(app)

tasks = {}
tasks['task_physics'] = TaskManagerGlobal.taskMgr.add(bullet_world.task_physics, 'task_physics')

# model_np = app.loader.load_model('./maze.obj')
# model_tex = app.loader.loadTexture('./maze_tex.png')
# geom = model_np.findAllMatches('**/+GeomNode').getPath(0).node().getGeom(0)
# mesh = BulletTriangleMesh()
# mesh.addGeom(geom)
# shape = BulletTriangleMeshShape(mesh, dynamic=False)
# np = app.render.attachNewNode(BulletRigidBodyNode('Mesh'))
# np.node().addShape(shape)
# np.setPos(0, 0, 2)
# np.setTexture(model_tex)
# # np.setCollideMask(BitMask32.allOn())
# bullet_world.attachRigidBody(np.node())
#
# etc.task_model_tilt_mouse_model    = model_np
# etc.task_model_tilt_mouse_x_coef   = 10
# etc.task_model_tilt_mouse_x_offset = 0
# etc.task_model_tilt_mouse_y_coef   = 10
# etc.task_model_tilt_mouse_y_offset = 0

from panda3d.core import ClockObject

def task_choose_box_rndm(task):
  dt = ClockObject.getGlobalClock().getDt()
  if random.random() >= 0.9 and random.random() >= 0.9:
    etc.task_cam_around_point_point = random.choice(boxes)
  return task.cont

bullet_world.plane(pos=(0, 0, -2))
boxes = []
for i in range(10, 100, 2):
  boxes.append(bullet_world.box(pos=(0, 0, i)))

etc.task_cam_around_point_point = random.choice(boxes)

tasks['task_choose_box_rndm']  = TaskManagerGlobal.taskMgr.add(task_choose_box_rndm, 'task_choose_box_rndm')

tasks['task_cam_around_point'] = TaskManagerGlobal.taskMgr.add(etc.task_cam_around_point, 'task_cam_around_point')
etc.task_cam_around_point_y_mul    = 1
etc.task_cam_around_point_r        = 20

if __name__ == '__main__':
  app.run()