from . import config_
config = config_.load()

from panda3d.bullet import *
from panda3d.core import ClockObject
from panda3d.core import Vec3
from direct.showbase.Loader import Loader


class world(BulletWorld):
  def __init__(self, app, gravity=config['bullet_gravity'], *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.setGravity(Vec3(*gravity))
    self.render = app.render
    self.app = app

  def task_physics(self, task):
    global_clock = ClockObject.getGlobalClock()
    dt = global_clock.getDt()
    self.doPhysics(dt)
    return task.cont

  def box(self, tex_path='models/box.egg', size=(1, 1, 1), mass=1.0, pos=(0, 0, 2)):
    shape = BulletBoxShape(Vec3(*size))
    node = BulletRigidBodyNode('Box')
    node.setMass(mass)
    node.addShape(shape)
    np = self.render.attachNewNode(node)
    np.setPos(*pos)
    self.attachRigidBody(node)
    model = Loader.loadModel(Loader(self.app), tex_path)
    model.flattenLight()
    model.reparentTo(np)
    return np
  
  def plane(self, tex_path='models', pos=(0, 0, 0)):
    shape = BulletPlaneShape(Vec3(0, 0, 1), 1)
    node = BulletRigidBodyNode('Ground')
    node.addShape(shape)
    np = self.render.attachNewNode(node)
    np.setPos(*pos)
    self.attachRigidBody(node)