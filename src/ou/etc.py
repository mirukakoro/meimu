from direct.gui.OnscreenText import OnscreenText
import math


class etc():
  def __init__(self, app, task_cam_around_point_r=10, task_cam_around_point_point=(0, 0, 0)):
    self.app                            = app
    self.task_cam_around_point_r        = task_cam_around_point_r
    self.task_cam_around_point_point    = task_cam_around_point_point
    self.task_cam_around_point_msg      = OnscreenText(text='', pos=(0, 0), scale=0.05)
    self.task_cam_around_point_x_mul    = 1
    self.task_cam_around_point_x_offset = 0
    self.task_cam_around_point_x_rot    = 0
    self.task_cam_around_point_y_mul    = 1
    self.task_cam_around_point_y_offset = 0
    self.task_cam_around_point_y_rot    = 0
    self.task_cam_around_point_msg_show = True
    self.task_model_tilt_mouse_model    = None
    self.task_model_tilt_mouse_x_coef   = 1
    self.task_model_tilt_mouse_x_offset = 0
    self.task_model_tilt_mouse_y_coef   = 1
    self.task_model_tilt_mouse_y_offset = 0
  def task_show_coord(self, task):
    print(self.app.camera.getPos())
    return task.cont
  def task_cam_around_point(self, task):
    # self.task_cam_around_point_x_rot = self.app.pointer.getX() / self.app.win.getXSize() # x_rotation
    # self.task_cam_around_point_y_rot = self.app.pointer.getY() / self.app.win.getYSize()
    if self.app.mouseWatcherNode.hasMouse():
      self.task_cam_around_point_x_rot = self.app.mouseWatcherNode.getMouseX() * self.task_cam_around_point_x_mul + self.task_cam_around_point_x_offset
      self.task_cam_around_point_y_rot = self.app.mouseWatcherNode.getMouseY() * self.task_cam_around_point_y_mul + self.task_cam_around_point_y_offset
    r = self.task_cam_around_point_r
    try:
      self.task_cam_around_point_point[0]
    except TypeError:
      x = r * math.cos(self.task_cam_around_point_y_rot) * math.cos(self.task_cam_around_point_x_rot) + self.task_cam_around_point_point.getPos()[0]
      y = r * math.cos(self.task_cam_around_point_y_rot) * math.sin(self.task_cam_around_point_x_rot) + self.task_cam_around_point_point.getPos()[1]
      z = r * math.sin(self.task_cam_around_point_y_rot) + self.task_cam_around_point_point.getPos()[2]
    else:
      x = r * math.cos(self.task_cam_around_point_y_rot) * math.cos(self.task_cam_around_point_x_rot) + self.task_cam_around_point_point[0]
      y = r * math.cos(self.task_cam_around_point_y_rot) * math.sin(self.task_cam_around_point_x_rot) + self.task_cam_around_point_point[1]
      z = r * math.sin(self.task_cam_around_point_y_rot) + self.task_cam_around_point_point[2]
    self.app.cam.setX(x)
    self.app.cam.setY(y)
    self.app.cam.setZ(z)
    self.app.cam.lookAt(self.task_cam_around_point_point)
    msg = '{} {} | {} | {} {} {}'.format(self.task_cam_around_point_x_rot, self.task_cam_around_point_y_rot, r, x, y, z)
    if self.task_cam_around_point_msg_show:
      self.task_cam_around_point_msg.setText(msg)
    return task.cont
  def task_cam_around_point_alt(self, task):
    # x_rot = self.app.pointer.getX() / self.app.win.getXSize() # x_rotation
    # y_rot = self.app.pointer.getY() / self.app.win.getYSize()
    if self.app.mouseWatcherNode.hasMouse():
      x_rot = self.app.mouseWatcherNode.getMouseX() * self.task_cam_around_point_x_mul + self.task_cam_around_point_x_offset
      y_rot = self.app.mouseWatcherNode.getMouseY() * self.task_cam_around_point_y_mul + self.task_cam_around_point_y_offset
      r = self.task_cam_around_point_r
      x = r*math.cos(y_rot)*math.cos(x_rot) + self.task_cam_around_point_point[0]
      y = r*math.cos(y_rot)*math.sin(x_rot) + self.task_cam_around_point_point[1]
      z = r*math.sin(y_rot)                 + self.task_cam_around_point_point[2]
      self.app.cam.setX(x)
      self.app.cam.setY(y)
      self.app.cam.setZ(z)
      self.app.cam.lookAt(self.task_cam_around_point_point)
      msg = '{} {} | {} | {} {} {}'.format(x_rot, y_rot, r, x, y, z)
    else:
      msg = 'no_mouse_to_use'
    if self.task_cam_around_point_msg_show:
      self.task_cam_around_point_msg.setText(msg)
    return task.cont
  # etc.task_cam_around_point_msg_show = True
  # etc.task_cam_around_point_x_mul = 0.5
  # etc.task_cam_around_point_x_offset = 0
  # etc.task_cam_around_point_y_mul = 0.5
  # etc.task_cam_around_point_y_offset = 90
  # tasks['task_cam_around_point_alt'] = TaskManagerGlobal.taskMgr.add(etc.task_cam_around_point_alt)
  def task_model_tilt_mouse(self, task):
    x_rot = self.app.mouseWatcherNode.getMouseX() * self.task_model_tilt_mouse_x_coef + self.task_model_tilt_mouse_x_offset
    y_rot = self.app.mouseWatcherNode.getMouseY() * self.task_model_tilt_mouse_y_coef + self.task_model_tilt_mouse_y_offset
    self.task_model_tilt_mouse_model.setP(y_rot)
    self.task_model_tilt_mouse_model.setR(x_rot)
    return task.cont