import json

def load(path='./config.json'):
  with open(path, 'r') as file:
    return json.load(file)

def save(data, path='./config.json'):
  with open(path, 'w') as file:
    json.dump(data, file)
    return 0
