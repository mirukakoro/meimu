from . import config_
config = config_.load()

from panda3d.core import loadPrcFile


def load(path=config['prc_path']):
  loadPrcFile(path)